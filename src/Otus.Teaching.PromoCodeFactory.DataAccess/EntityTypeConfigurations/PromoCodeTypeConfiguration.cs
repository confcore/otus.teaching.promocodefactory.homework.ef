﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.EntityTypeConfigurations
{
    public class PromoCodeTypeConfiguration : IEntityTypeConfiguration<PromoCode>
    {
        public void Configure(EntityTypeBuilder<PromoCode> builder)
        {
            builder.HasKey(p => p.Id);

            builder
                .HasOne(p => p.Customer)
                .WithMany(c => c.PromoCodes)
                .OnDelete(DeleteBehavior.Cascade);

            builder.Property(p => p.Code).IsRequired().HasMaxLength(10);
            builder.Property(p => p.ServiceInfo).IsRequired().HasMaxLength(100);
            builder.Property(p => p.PartnerName).IsRequired().HasMaxLength(50);
        }
    }
}
