﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T : BaseEntity
    {
        private readonly List<T> _data;

        protected IEnumerable<T> Data => _data;

        public InMemoryRepository(IEnumerable<T> data)
        {
            _data = data.ToList();
        }

        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<IEnumerable<T>> GetAllAsync(Expression<Func<T, bool>> predicate)
        {
            return Task.FromResult(Data.Where(predicate.Compile()));
        }

        public Task<T> FindAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task AddAsync(T item)
        {
            _data.Add(item);

            return Task.FromResult(item);
        }

        public Task UpdateAsync(T item)
        {
            var updateItem = _data.Where(i => i.Id == item.Id).FirstOrDefault();
            _data.Remove(updateItem);
            _data.Add(item);

            return Task.FromResult(item);
        }

        public Task DeleteAsync(T item)
        {
            _data.Remove(item);

            return Task.FromResult(item);
        }

        public Task AddRangeAsync(IEnumerable<T> entities)
        {
            _data.AddRange(entities);

            return Task.FromResult(entities);
        }

        public Task DeleteRangeAsync(IEnumerable<T> entities)
        {
            _data.RemoveAll(e => entities.Contains(e));

            return Task.FromResult(entities);
        }
    }
}