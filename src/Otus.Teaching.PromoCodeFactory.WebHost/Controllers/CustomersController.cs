﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты.
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IRepository<CustomerPreference> _customerPreferenceRepository;
        private readonly IRepository<PromoCode> _promoCodeRepository;

        public CustomersController(
            IRepository<Customer> customerRepository,
            IRepository<Preference> preferenceRepository,
            IRepository<CustomerPreference> customerPreferenceRepository,
            IRepository<PromoCode> promoCodeRepository
            )
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
            _customerPreferenceRepository = customerPreferenceRepository;
            _promoCodeRepository = promoCodeRepository;
        }

        /// <summary>
        /// Получить всех клиентов.
        /// </summary>
        /// <returns>Клиенты.</returns>
        [HttpGet]
        public async Task<ActionResult<CustomerShortResponse>> GetCustomersAsync()
        {
            var customers = await _customerRepository.GetAllAsync();

            var response = customers
                .Select(c => CustomerStaticMapper.MapToCustomerShortResponse(c))
                .ToList();

            return Ok(response);
        }

        /// <summary>
        /// Найти клиента.
        /// </summary>
        /// <param name="id">Идентификатор клиента.</param>
        /// <returns>Клиент.</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await _customerRepository.FindAsync(id);
            if (customer == null)
            {
                return NotFound($"Customer with id {id} not found.");
            }

            var customerPreferences = await _customerPreferenceRepository.GetAllAsync(cp => cp.CustomerId == id);
            var preferenceIds = customerPreferences.Select(cp => cp.PreferenceId).ToList();
            var preferences = await _preferenceRepository.GetAllAsync(p => preferenceIds.Contains(p.Id));
            var promoCodes = await _promoCodeRepository.GetAllAsync(pc => pc.CustomerId == id);

            var response = CustomerStaticMapper.MapToCustomerResponse(customer, preferences, promoCodes);

            return Ok(response);
        }

        /// <summary>
        /// Создать клиента.
        /// </summary>
        /// <param name="request">Запрос на создание клиента.</param>
        /// <returns>Результат операции.</returns>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            var customer = CustomerStaticMapper.MapToCustomer(request);
            var preferences = await _preferenceRepository.GetAllAsync(p => request.PreferenceIds.Contains(p.Id));
            customer.Preferences = preferences.Select(cp => new CustomerPreference()
            {
                CustomerId = customer.Id,
                PreferenceId = cp.Id
            }).ToList();

            await _customerRepository.AddAsync(customer);

            return Ok();
        }

        /// <summary>
        /// Изменить клиента.
        /// </summary>
        /// <param name="id">Идентификатор клиента.</param>
        /// <param name="request">Запрос на изменение клиента.</param>
        /// <returns>Результат операции.</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await _customerRepository.FindAsync(id);
            if (customer == null)
            {
                return NotFound($"Customer with id {id} not found.");
            }

            CustomerStaticMapper.MapToCustomer(request, customer);
            var preferences = await _preferenceRepository.GetAllAsync(p => request.PreferenceIds.Contains(p.Id));
            var newCustomerPreferences = preferences.Select(cp => new CustomerPreference()
            {
                CustomerId = id,
                PreferenceId = cp.Id
            }).ToList();

            var customerPreferences = await _customerPreferenceRepository.GetAllAsync(cp => cp.CustomerId == id);
            await _customerPreferenceRepository.DeleteRangeAsync(customerPreferences);
            await _customerPreferenceRepository.AddRangeAsync(newCustomerPreferences);

            await _customerRepository.UpdateAsync(customer);

            return Ok();
        }

        /// <summary>
        /// Удалить клиента.
        /// </summary>
        /// <param name="id">Идентификатор клиента.</param>
        /// <returns>Результат операции.</returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            var customer = await _customerRepository.FindAsync(id);
            if (customer == null)
            {
                return NotFound($"Customer with id {id} not found.");
            }

            await _customerRepository.DeleteAsync(customer);

            return Ok();
        }
    }
}