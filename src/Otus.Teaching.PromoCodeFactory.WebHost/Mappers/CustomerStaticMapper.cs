﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mappers
{
    public static class CustomerStaticMapper
    {
        public static CustomerShortResponse MapToCustomerShortResponse(Customer customer)
        {
            return new CustomerShortResponse()
            {
                Id = customer.Id,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Email = customer.Email
            };
        }

        public static CustomerResponse MapToCustomerResponse(Customer customer, IEnumerable<Preference> preferences, IEnumerable<PromoCode> promoCodes)
        {
            return new CustomerResponse()
            {
                Id = customer.Id,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Email = customer.Email,
                Preferences = preferences.Select(PreferenceStaticMapper.MapToPreferenceResponse).ToList(),
                PromoCodes = promoCodes.Select(pc => new PromoCodeShortResponse()
                {
                    Id = pc.Id,
                    BeginDate = pc.BeginDate.ToString(),
                    EndDate = pc.EndDate.ToString(),
                    Code = pc.Code,
                    PartnerName = pc.PartnerName,
                    ServiceInfo = pc.ServiceInfo
                }).ToList()
            };
        }

        public static Customer MapToCustomer(CreateOrEditCustomerRequest request, Guid id = default)
        {
            return new Customer()
            {
                Id = id,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
            };
        }

        public static void MapToCustomer(CreateOrEditCustomerRequest request, Customer customer)
        {
            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;
            customer.Email = request.LastName;
        }
    }
}
